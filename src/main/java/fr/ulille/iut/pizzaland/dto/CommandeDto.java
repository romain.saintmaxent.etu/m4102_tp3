package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {

	private UUID id;
	private String prenom;
    private String name;
	private List<Pizza> pizzas;

    public CommandeDto() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Pizza> getPizza() {
		return pizzas;
	}

	public void setPizza(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
}
