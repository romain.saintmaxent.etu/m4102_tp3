package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;


public class Pizza {
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;

	public Pizza() {
	}

	public Pizza(String name, List<Ingredient> ingredients) {
		this.name = name;
		this.ingredients=ingredients;
	}

	public Pizza(UUID id, String name) {
		this.id = id;
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());
        dto.setIngredients(i.getIngredients());
        
		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizzas = new Pizza();
		pizzas.setId(dto.getId());
		pizzas.setName(dto.getName());

		return pizzas;
	}

	@Override
	public String toString() {
		return "Pizzas [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizzas) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizzas.getName());

		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizzas = new Pizza();
		pizzas.setName(dto.getName());

		return pizzas;
	}

}