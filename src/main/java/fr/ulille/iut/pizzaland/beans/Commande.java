package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	
	private UUID id = UUID.randomUUID();
	private String prenom;
	private String name;
	private List<Pizza> pizzas;

	public Commande() {
	}

	public Commande(String prenom, String name, List<Pizza> pizzas) {
		this.prenom = prenom;
		this.name = name;
		this.pizzas=pizzas;
	}

	public Commande(UUID id, String prenom, String name) {
		this.id = id;
		this.prenom = prenom;
		this.name = name;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Commande [id=" + id + ", prenom=" + prenom + ", name=" + name + ", pizzas=" + pizzas + "]";
	}

	public static CommandeDto toDto(Commande i) {
		CommandeDto dto = new CommandeDto();
		dto.setId(i.getId());
		dto.setPrenom(i.getPrenom());
		dto.setName(i.getName());
		dto.setPizza(i.getPizzas());
		
		return dto;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande commande = new Commande();
		commande.setId(dto.getId());
		commande.setPrenom(dto.getPrenom());
		commande.setName(dto.getName());

		return commande;
	}

	public static CommandeCreateDto toCreateDto(Commande commande) {
		CommandeCreateDto dto = new CommandeCreateDto();
		dto.setName(commande.getName());
		dto.setPrenom(commande.getPrenom());

		return dto;
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
		Commande commande = new Commande();
		commande.setName(dto.getName());
		commande.setPrenom(dto.getPrenom());

		return commande;
	}

	public Commande findById(UUID id2) {
		// TODO Auto-generated method stub
		return null;
	}

}