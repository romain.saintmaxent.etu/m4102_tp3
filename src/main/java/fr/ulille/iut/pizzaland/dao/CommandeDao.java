package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;

public interface CommandeDao {
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (pizzaID VARCHAR(128), ingredientID VARCHAR(128), PRIMARY KEY(pizzaID,ingredientID))")
	void createAssociationTable();

	@Transaction
	default void createTableAndPizzasAssociation() {
		createAssociationTable();
		createCommandeTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS commande")
	void dropCommandeTable();

	@SqlUpdate("INSERT INTO commande (id, name) VALUES (:id, :name)")
	void insertCommande(@BindBean Commande commande);

	@SqlUpdate("DELETE FROM commande WHERE id = :id")
	void removeCommande(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM commande WHERE name = :name")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM commande")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();

	@SqlQuery("SELECT * FROM commande WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(@Bind("id") UUID id);

}
